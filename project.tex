\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Paraconsistency and Word Puzzles}
\author{TIANTIAN GAO, PAUL FODOR and MICHAEL KIFER}

\date{December 2018}

\begin{document}

\maketitle

Project by Mattew Weston

\section{Summary of Paper}

One of the most difficult problems to solve in dealing with representations of natural language in logic programming is the presence of inconsistent information. In \textit{Paraconsistency and Word Puzzles}, Tiantian Gao and Professors Fodor and Kifer propose a set of Principles for encoding natural language problems into answer set programming using Annotated Predicate Calculus, or APC. Through the introduction of a new type of non-monotonic semantics for APC referred to as consistency preferred stable models, they are able to describe a process by which natural language statements can, in conjunction with the relevant background information, be translated into Answer Set Programming in a way that allows the resulting program to detect and address any inconsistent information. Throughout the history of logic programming, the use of word puzzles to model issues dealing with natural language has proven to be quite useful. Thus, the paper demonstrates its proposed methods using three well known word puzzles, implementing each of them in Clingo and its add-on, Aspirin, and testing the resulting programs with a variety of different kinds of inconsistencies.

\section{Goal and Significance}

In demonstrating that it is now possible to examine Paraconsistency using only Clingo, we intend to further validate the strategies described in Paraconsistency and Word Puzzles and make it easier for future research to be carried out in this area. By eliminating the need to use Aspirin to augment the capabilities of Clingo, we hope to make gains in two major areas, both of which carry significant implications for the paper’s findings.

The first of these improvements is that, by reducing the scale and complexity of the setup, we will be able to make the process significantly more accessible, and thus more likely to be subject to further research. Aspirin is a moderately obscure framework, and even experienced developers may require a modest amount of time to install and configure Aspirin and Clingo to work together on a new machine. In contrast, Clingo is very widely used, and can even be run through various publicly available web services, including the one provided by Potassco itself. Should we be able to demonstrate that Clingo is sufficient to test the methods designed in the paper, we will vastly increase the number of people with the means to translate additional word problems, giving us the potential for much greater insight into how the Principles enumerated in the paper work, and whether any improvements to them can be made.

The second major gain from switching from Clingo and Asprin to pure Clingo is that we reduce the number of potential sources of problems. As a known issue with the setup used in the original paper is the presence of technical issues associated with running the resulting programs, often resulting in inaccurate results, the ability to either eliminate these issues or narrow down the set of potential causes is extremely useful. For every additional technology that is required in a system, the number of ways something could go wrong increases logarithmically. Issues can arise from incompatibilities between any two pieces of software being used together, which is an obvious concern. An update to Aspirin may cause issues when used in conjunction with a given version of Clingo, for example, as well as the other way around. In addition, problems associated with the current build of any one program in a development stack can cascade through the entire stack, causing issues that seem inexplicable and are a challenge both to detect and to resolve. Furthermore, writing code with multiple dependencies requires proficiency in all of them, making it significantly more difficult to avoid introducing bugs.

\section{Process}

Over the course of this paper, we will attempt to implement the marathon puzzle in pure Clingo using the techniques described in Paraconsistency and Word Puzzles. By doing this, we not only demonstrate the viability of our methods, but also provide an example for future researchers to follow when implementing Paraconsistent language in Clingo.
\newline
\newline
For reference, we provide a copy of the original marathon puzzle:

\begin{enumerate}
\item Dominique, Ignace, Naren, Olivier, Philippe, and Pascal have arrived as the first six at the Paris marathon.
\item Olivier has not arrived last.
\item Dominique, Pascal and Ignace have arrived before Naren and Olivier.
\item Dominique who was third last year has improved this year.
\item Philippe is among the first four.
\item Ignace has arrived neither in second nor third position.
\item Pascal has beaten Naren by three positions.
\item Neither Ignace nor Dominique are in the fourth position.
\end{enumerate}

The goal of this puzzle is to assign each of the six runners a position that is consistent with the information provided. Of course, once we introduce inconsistent information, the true test of our system will be its ability to answer elegantly despite the absence of an entirely consistent ordering.
\newline
\newline
The solution to the unmodified puzzle is the following ordering:

\begin{enumerate}
\item Ignace
\item Dominique
\item Pascal
\item Philippe
\item Olivier
\item Naren
\end{enumerate}

One necessary casualty of the translation is the truth functor, due to Clingo’s reliance on tuples rather than functors. This means that some aspects of the translation must be adjusted. Instead of using Truth(X, v), where v is the truth value of X, each concept will be expressed as a tuple, with the first set of parameters representing the variables associated with an instance of the concept and the last variable being the truth value associated with that concept and those variables. The overall strategies used remain correct, as the Principles outlined in the paper are not restricted to a given language.

\section{Encoding Background Information}

We begin by encoding our background information on the problem into Clingo. As stated in the paper, this information is as follows:

\begin{enumerate}
\item No two runners arrive at the same time.
\item A runner arrives before another runner if the former has a lower position value than the latter.
\end{enumerate}

The first thing we can do is encode the 1-1 correspondence between runners and positions using Principles five and six. Principle five, complete knowledge, allows us to mandate that a given piece of information be defined completely, never being unknown. We do this by checking whether a relationship with valid arguments is true, and setting it to false if not, thus making a closed world assumption. Principle six, exactly N, allows us to ensure that no two runners are assigned to the same position and vice versa. This is done by stating that every has\_position relationship that involves a given position is only true for exactly one runner, and that every has\_position relationship that involves a given runner is only true for one position.
\newline
\newline
\%  Each runner has exactly one position and vice versa\newline
1 \{ has\_position( R, P, t) : position(P, t) \} 1 :- runner(R, t).\newline
1 \{ has\_position(R,P, t) : runner(R, t) \} 1 :- position(P, t).\newline
\newline
\% we have complete knowledge of runners' positions\newline
has\_position(R, P, f) :- \newline
	runner(R, t),\newline
	position(P, t),\newline
	not has\_position(R, P, t).
\newline
\newline
Defining ‘before’ using pure Clingo uses Principles two and five. Principle two, the propagation of inconsistency, allows us to block conclusions drawn from inconsistent information by ensuring that we check whether a fact that we are using in our logic is inconsistent. For this step, we avoid building ‘before’ relationships based on ‘has\_position’ relationships if the latter are inconsistent. This stops inconsistency from propagating through our models, giving us a much more useful set of results from the program.
\newline
\newline
\% First, we translate the meaning of 'before' into ASP using Principles two and five.\newline
before(R1, R2, t) :- \newline
	runner(R1, t), \newline
	runner(R2, t), \newline
	has\_position(R1, P1, t),\newline
	position(P1, t), \newline
	has\_position(R2, P2, t),\newline
	position(P2, t), \newline
	P1 $<$ P2,\newline
	not has\_position(R1, P1, top),\newline
	not has\_position(R2, P2, top).\newline
\newline
\% We can say that the reverse is false with the exact same criteria, naturally.
before(R2, R1, f) :- \newline
	runner(R1, t), \newline
	runner(R2, t), \newline
	has\_position(R1, P1, t),\newline
	position(P1, t), \newline
	has\_position(R2, P2, t),\newline
	position(P2, t), \newline
	P1 $<$ P2,\newline
	not has\_position(R1, P1, top),\newline
	not has\_position(R2, P2, top).\newline
\newline
\% Guarantee complete knowledge for before.
before(R1, R2, f) :-\newline
	runner(R1, t),\newline
	runner(R2, t),\newline
	not before(R1, R2, t).\newline

\subsection{Encoding the First Statement}

To encode the first statement is a matter of enumerating the runners and positions provided by the problem. This is a fairly straightforward process, as shown below - we list each runner and each position, thus defining the domain of our puzzle.
\newline
\newline
\% 1. Dominique, Ignace, Naren, Olivier, Philippe, and Pascal have arrived as the first six at the Paris marathon.\newline
\% Enumerate Runners\newline
runner(dominique, t).\newline
runner(ignace, t).\newline
runner(naren, t).\newline
runner(olivier, t).\newline
runner(pascal, t).\newline
runner(philippe, t).\newline
\newline
\% Enumerate Positions\newline
position(1, t).\newline
position(2, t).\newline
position(3, t).\newline
position(4, t).\newline
position(5, t).\newline
position(6, t).\newline

\subsection{Encoding the Second Statement}

The second statement, that Olivier has not arrived last, is straightforward to encode. We simply state that the has\_position relationship between Olivier and position 6 is false.
\newline\newline
\% 2. Olivier has not arrived last.\newline
has\_position(olivier, 6, f).\newline

\subsection{Encoding the Third Statement}

The third statement is similarly straightforward to encode, since we defined the ‘before’ relation above. The third statement tells us that Dominique, Pascal, and Ignace all arrived before Naren and Olivier. This statement means, in effect, that a collection of six different ‘before’ relationships are all true - each runner in the set {Dominique, Pascal, Ignace} arrived before Pascal, and each of these people also arrived before Olivier.
\newline\newline
\% 3. Dominique, Pascal, and Ignace have arrived before Naren and Olivier.\newline
before(dominique, naren, t).\newline
before(pascal, naren, t).\newline
before(ignace, naren, t).\newline
\newline\newline
before(dominique, olivier, t).\newline
before(pascal, olivier, t).\newline
before(ignace, olivier, t).\newline

\subsection{Encoding the Fourth Statement}

The fourth statement says that Dominique has improved over his position of three last year. This means that either he holds position one, or he holds position two.
\newline\newline
\% 4. Dominique, who was third last year, has improved this year.\newline
has\_position(dominique, 1, t) ; has\_position(dominique, 2, t).\newline

\subsection{Encoding the Fifth Statement}

The fifth statement can be encoded in much the same manner as the fourth - the knowledge that Philippe is among the first four runners means that, of the associations between Philippe and the first four positions, at least one must be true.
\newline\newline
\% 5. Phillipe is among the first four.\newline
has\_position(philippe, 1, t) ; has\_position(philippe, 2, t) ; has\_position(philippe, 3, t) ; has\_position(philippe, 4, t).\newline

\subsection{Encoding the Sixth Statement}

The sixth statement, which says that Ignace has arrived in neither second nor third position, means that the has\_position relationships between Ignace and positions 3 and 4 both hold a truth value of ‘false’.
\newline\newline
\% 6. Ignace has arrived neither in second nor third position.\newline
has\_position(ignace, 2, f).\newline
has\_position(ignace, 3, f).\newline

\subsection{Encoding the Seventh Statement}

The seventh statement, which states that Pascal has beaten Naren by exactly 3 positions, is encoded using Principle 2. Our encoding simply states that, if we know of a true has\_position relationship for Pascal, the has\_position relationship between Naren and the position three positions higher must also hold value true. It also states that, if we know of a true has\_position relationship for Naren, the has\_position relationship between Pascal and the position three values lower must hold true.
\newline\newline
\% 7. Pascal has beaten Naren by three positions\newline
has\_position(naren, P2, t) :- \newline
	has\_position(pascal, P1, t),\newline
	position(P1, t),\newline
	position(P2, t),\newline
	P1 + 3 = P2,\newline
	not position(P1, top),\newline
	not position(P2, top),\newline
	not has\_position(pascal, P1, top).\newline
\newline
has\_position(pascal, P1, t) :- \newline
	has\_position(naren, P2, t),\newline
	position(P1, t),\newline
	position(P2, t),\newline
	P1 + 3 = P2,\newline
	not position(P1, top),\newline
	not position(P2, top),\newline
	not has\_position(pascal, P1, top).

\subsection{Encoding the Eighth Statement}

The eighth statement, much like the sixth, simply tells us that a set of has\_position relationships have value false. It can thus be encoded in the same straightforward manner.
\newline\newline
\% 8. Neither ignace nor dominique are in the fourth position.\newline
has\_position(ignace, 4, f).\newline
has\_position(dominique, 4, f).

\section{Testing}

Now that we have successfully translated the marathon problem into pure Clingo in accordance with the Principles outlined in the paper, we can test our code to determine how successful we were in doing so. All testing performed in this paper is carried out using Potassco’s online Clingo service, which can be found at http://potassco.sourceforge.net/clingo.html.
\newline\newline
To aid in testing, we write some additional Clingo code to provide us with an output consisting of the has\_position assignments made and the inconsistencies in both the before and has\_position relationships present in a given solution.
\newline\newline
has\_position(R,P) :-\newline
  has\_position(R,P,t).\newline
\#show has\_position/2.\newline
\newline
top\_position(R,P) :-\newline
  has\_position(R,P,top).\newline
\#show top\_position/2.\newline
\newline
top\_before(R1,R2) :-\newline
  before(R1,R2,top).\newline
\#show top\_before/2.\newline

Before anything else, it makes sense to ensure that our implementation is accurate when no inconsistencies are present. By starting with the default problem and checking for accuracy before delving further into testing, we are able to determine whether there are any glaring issues that must immediately be addressed. To do this, we add two new restrictions that forbid inconsistency and run our code for the unmodified marathon puzzle.
\newline\newline
:- top\_before(R1, R2).\newline
:- top\_position(R, P).\newline
\newline
The result of this test is as follows:
\newline\newline
clingo version 5.0.0\newline
Solving...\newline
Answer: 1\newline
has\_position(dominique,2) has\_position(ignace,1) has\_position(naren,6) has\_position(olivier,5) has\_position(pascal,3) has\_position(philippe,4)\newline
SATISFIABLE\newline
\newline
Models       : 1\newline
Calls        : 1\newline
Time         : 0.026s (Solving: 0.00s 1st Model: 0.00s Unsat: 0.00s)\newline
CPU Time     : 0.000s\newline
\newline
As we would expect, the program accurately solves the standard marathon puzzle. Next, we can perform some more interesting tests, in order to explore how our pure Clingo encoding handles inconsistency. To begin, we’ll look at the first variation run in the paper, adding the inconsistent statement that Pascal arrives in position 6.
\newline
\newline
has\_position(pascal, 6, t).
\newline
\newline
This yields 16 results across all levels of inconsistency. The least inconsistent result matches what was calculated in the original paper - the ordering Ignace, Dominique, Nare, Philippe, Olivier, Pascal. Encouragingly, the program detects that the inconsistencies present in this ordering come from two of the six before relationships provided in the third statement of the puzzle, which cannot be true if we place Pascal, who is supposed to be before Olivier and Naren, at the last available position, 6.
\newline
\newline
has\_position(pascal,6) has\_position(dominique,2) has\_position(ignace,1) has\_position(naren,3) has\_position(olivier,5) has\_position(philippe,4) top\_before(pascal,olivier) top\_before(pascal,naren)
\newline
\newline
Running the remaining two tests from the paper yields similarly promising results. Adding the inconsistent statement has\_position(ignace, 2, t) yields 3 results across all levels of inconsistency, with the least inconsistent among them being the same as the result obtained in the paper - the ordering Dominique, Ignace, Pascal, Philippe, Olivier, Naren, with the sole inconsistency being the value of the has\_position relationship between Ignace and position 2. Adding the inconsistent statement before(philippe, dominique, t) yields 54 results across all levels of inconsistency, with the four results best matching the preferences described in the paper showing the same inconsistencies, shown below:
\newline
\newline
has\_position(dominique,2) has\_position(ignace,1) has\_position(naren,6) has\_position(olivier,5) has\_position(pascal,3) has\_position(philippe,4) top\_before(philippe,dominique)
\newline
\newline
has\_position(dominique,2) has\_position(ignace,4) has\_position(naren,6) has\_position(olivier,5) has\_position(pascal,3) has\_position(philippe,1) top\_position(ignace,4)
\newline
\newline
has\_position(dominique,2) has\_position(ignace,5) has\_position(naren,6) has\_position(olivier,4) has\_position(pascal,3) has\_position(philippe,1) top\_before(ignace,olivier)
\newline
\newline
has\_position(dominique,2) has\_position(ignace,5) has\_position(naren,3) has\_position(olivier,6) has\_position(pascal,4) has\_position(philippe,1) top\_position(olivier,6) top\_before(pascal,naren) top\_before(ignace,naren)

\section{Conclusion:}

In this paper, we covered the translation of word puzzle into pure Clingo using the Principles outlined in Paraconsistency and Word Puzzles. The successful translation of the marathon word puzzle into pure Clingo is promising on two levels. Firstly, it shows that the Principles enumerated and demonstrated in the paper are adaptable, and that successful use of these Principles does not require the same setup as the original paper used. Secondly, it makes it significantly easier to test, utilize, and build upon these Principles, as setup is no longer an extensive, potentially error-prone process, and researchers interested in paraconsistency can solve paraconsistency problems using only the online service used in this paper. Furthermore, we show that the results obtained when using Clingo encoding are both accurate and consistent with those obtained using a Clingo-Aspirin encoding.

Future work could involve the use of weights to specify which inconsistencies are more tolerable than others, allowing us to more easily examine our sets of models for useful information. In addition, the encoding of word puzzles in other ASP languages could further demonstrate the versatility of the Principles discussed, and provide future researchers with valuable examples of encodings to reference when performing encodings.

\section*{References}

GAO, T., FODOR, P., \& KIFER, M. (2016). Paraconsistency and word puzzles. Theory and Practice of Logic Programming, 16(5-6), 703-720. doi:10.1017/S1471068416000326

\end{document}
