# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is my CSE 505 final project.
* It is an encoding of the marathon puzzle into clingo in a manner that handles Paraconsistency

### How do I get set up? ###

* One of the major advantages of using pure Clingo is that compilation can be done entirely online
* One service that allows for this can be found at http://potassco.sourceforge.net/clingo.html
* All scripts here can be copied and pasted into the textfield at the above URL. No setup required!

### Who do I talk to? ###

* This repository was created by Matthew Weston