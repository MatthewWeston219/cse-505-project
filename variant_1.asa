% 0. BACKGROUND KNOWLEDGE

% Using principles 5 and 6, we provide some background information.
% In this case, the knowledge that each runner has exactly one position.

% Each runner has exactly one position and vice versa
1 { has_position( R, P, t) : position(P, t) } 1 :- runner(R, t).
1 { has_position(R,P, t) : runner(R, t) } 1 :- position(P, t).

% we have complete knowledge of runners' positions
has_position(R, P, f) :- 
	runner(R, t),
	position(P, t),
	not has_position(R, P, t).

% Now, we translate the meaning of 'before' into ASP using principles two and five.

before(R1, R2, t) :- 
	runner(R1, t), 
	runner(R2, t), 
	has_position(R1, P1, t),
	position(P1, t), 
	has_position(R2, P2, t),
	position(P2, t), 
	P1 < P2,
	not has_position(R1, P1, top),
	not has_position(R2, P2, top).

% We can say that the reverse is false with the exact same criteria, naturally.
before(R2, R1, f) :- 
	runner(R1, t), 
	runner(R2, t), 
	has_position(R1, P1, t),
	position(P1, t), 
	has_position(R2, P2, t),
	position(P2, t), 
	P1 < P2,
	not has_position(R1, P1, top),
	not has_position(R2, P2, top).

% Guarantee complete knowledge for before.
before(R1, R2, f) :-
	runner(R1, t),
	runner(R2, t),
	not before(R1, R2, t).
	
% Handle truth and falsehood.
% Because we've removed truth(X, v), we must address both has_position
% and before
has_position(R, P, top) :- has_position(R, P, t), has_position(R, P, f).
has_position(R, P, t) :- has_position(R, P, top).
has_position(R, P, f) :- has_position(R, P, top).

before(R1, R2, top) :- before(R1, R2, t), before(R1, R2, f).
before(R1, R2, t) :- before(R1, R2, top).
before(R1, R2, f) :- before(R1, R2, top).

% 1. Dominique, Ignace, Naren, Olivier, Philippe, and Pascal have arrived as the first six at the Paris marathon.
% Enumerate Runners
runner(dominique, t).
runner(ignace, t).
runner(naren, t).
runner(olivier, t).
runner(pascal, t).
runner(philippe, t).

% Enumerate Positions
position(1, t).
position(2, t).
position(3, t).
position(4, t).
position(5, t).
position(6, t).

% 2. Olivier has not arrived last.
has_position(olivier, 6, f).

% Each of the next few statements is straightforward to encode.

% 3. Dominique, Pascal, and Ignace have arrived before Naren and Olivier.
before(dominique, naren, t).
before(pascal, naren, t).
before(ignace, naren, t).

before(dominique, olivier, t).
before(pascal, olivier, t).
before(ignace, olivier, t).

% 4. Dominique, who was third last year, has improved this year.
has_position(dominique, 1, t) ; has_position(dominique, 2, t).

% 5. Phillipe is among the first four.
has_position(philippe, 1, t) ; has_position(philippe, 2, t) ; has_position(philippe, 3, t) ; has_position(philippe, 4, t).

% 6. Ignace has arrived neither in second nor third position.
has_position(ignace, 2, f).
has_position(ignace, 3, f).

% We encode the next sentence using principle 2.
% Our means of solving this depends on what we know.
% If we know Pascal's position, we can use it to determine Naren's.
% Likewise, if we know Naren's position, we can use it to determine Pascal's.

% 7. Pascal has beaten Naren by three positions
has_position(naren, P2, t) :- 
	has_position(pascal, P1, t),
	position(P1, t),
	position(P2, t),
	P1 + 3 = P2,
	not position(P1, top),
	not position(P2, top),
	not has_position(pascal, P1, top).

has_position(pascal, P1, t) :- 
	has_position(naren, P2, t),
	position(P1, t),
	position(P2, t),
	P1 + 3 = P2,
	not position(P1, top),
	not position(P2, top),
	not has_position(pascal, P1, top).

% 8. Neither ignace nor dominique are in the fourth position.
has_position(ignace, 4, f).
has_position(dominique, 4, f).

% Handle output

has_position(R,P) :-
  has_position(R,P,t).

#show has_position/2.

top_position(R,P) :-
  has_position(R,P,top).

#show top_position/2.

top_before(R1,R2) :-
  before(R1,R2,top).

#show top_before/2.

% Test
has_position(pascal, 6, t).